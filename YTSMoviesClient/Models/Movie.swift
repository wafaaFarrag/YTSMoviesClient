//
//  Movie.swift
//  YTSMoviesClient
//
//  Created by Wafaa Farrag  on 12/28/17.
//  Copyright © 2017 Wafaa Farrag . All rights reserved.
//

import Foundation
import Moya
import SwiftyJSON


class Movie {
    
    var id = 0
    var url = ""
    var mediumCoverImage = ""
    var title = ""
    var titleEnglish = ""
    var slug = ""
    var year = ""
    var rating = 0
    var dateUploaded = ""
    var summary = ""
    var descriptionFull = ""


    init() {
        
    }
  
     init(fromJson data : JSON) {
        self.id = data["id"].intValue
        self.url = data["url"].stringValue
        self.title = data["title"].stringValue
        self.titleEnglish = data["title_english"].stringValue
        self.slug = data["slug"].stringValue
        self.year = data["year"].stringValue
        self.rating = data["rating"].intValue
        self.mediumCoverImage = data["medium_cover_image"].stringValue
        self.dateUploaded = data["date_uploaded"].stringValue
        self.summary = data["summary"].stringValue
        self.descriptionFull = data["description_full"].stringValue
        
        
    }
    
    
    
}


