//
//  HelperMethods.swift
//  YTSMoviesClient
//
//  Created by Wafaa Farrag  on 1/4/18.
//  Copyright © 2018 Wafaa Farrag . All rights reserved.
//

import Foundation
import UIKit
import SystemConfiguration


class HelperMethods
{
    
    /// get type of device
    ///
    /// - Returns: return 1 for iphone and 2 for ipad
    class func getTypeOfDevice ()->Int
    {
        var type = 1
        switch UIDevice.current.userInterfaceIdiom {
        case .phone:
            type = 1
        case .pad:
            type = 2
        case .unspecified:
            type = 3
        default:
            break
        }
        return type
    }
    
}


