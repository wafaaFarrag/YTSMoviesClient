//
//  MoviesViewModel.swift
//  YTSMoviesClient
//
//  Created by Wafaa Farrag  on 12/28/17.
//  Copyright © 2017 Wafaa Farrag . All rights reserved.
//

import Foundation
import KRPullLoader
import SwiftyJSON
import Alamofire



class MoviesViewModel : NSObject {
    
    var moviesArray = [Movie]()
    var currentMovieArray : [Movie]?
    var page = 1


    
}




// MARK: - network
extension MoviesViewModel
{
    
    
    func searchWithQuery( queryTerm : String , completion :@escaping (_ movieCount: Int)->() , error :@escaping ()->())  {
        provider.request(.searchMovie(page : page , query_term : queryTerm)) { result in
            if case let .success(response) = result {
                let json = try? response.mapJSON()
                if let dictionary = json as? NSDictionary {
                    if let movies = dictionary.value(forKeyPath: "data.movies") as? NSArray{
                        self.currentMovieArray = [Movie]()
                        movies.forEach({
                            self.currentMovieArray?.append(Movie.init(fromJson: JSON.init($0)))
                        })
                        completion((dictionary.value(forKeyPath: "data.movie_count") as? Int)!)
                    }
                    else
                    {
                       completion((dictionary.value(forKeyPath: "data.movie_count") as? Int)!)
                    }
            }
        }
        else
        {
                print(result)
                error()
        }
            
    }
}
    
    func getMovies(completion :@escaping (_ movieCount: Int)->() , error :@escaping ()->() , refresh : Bool = false ){
        provider.request(.listMovies(page : self.page)) { result in
            var message = "Couldn't access API"
            if case let .success(response) = result {
                
                let jsonString = try? response.mapString()
                message = jsonString ?? message
                let json = try? response.mapJSON()
                if let dictionary = json as? NSDictionary {
                    if let movies = dictionary.value(forKeyPath: "data.movies") as? NSArray{
                        if refresh {
                             self.moviesArray.removeAll() 
                        }
                        movies.forEach({
                            self.moviesArray.append(Movie.init(fromJson: JSON.init($0)))
                            })
                        completion((dictionary.value(forKeyPath: "data.movie_count") as? Int)!)
                    }
                    else
                    {
                        completion((dictionary.value(forKeyPath: "data.movie_count") as? Int)!)
                    }
                }
                
            }
            else
            {
                error()
            }
        }
        
    }
    
    
    
    /// search in local movies array
    func searchByUsingFilter(query : String? ,completion :@escaping ()->()) {
        self.currentMovieArray = self.moviesArray.filter( { movie -> Bool in
            guard  let text = query
                else {return false}
            return movie.title.contains(text)})
            completion()
    }
    
    
   
}

