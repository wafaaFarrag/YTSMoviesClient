//
//  MoviesAPI.swift
//  YTSMoviesClient
//
//  Created by Wafaa Farrag  on 1/4/18.
//  Copyright © 2018 Wafaa Farrag . All rights reserved.
//

import Foundation
import Moya



let provider = MoyaProvider<service>()

public enum service
{
    case listMovies(page : Int)
    case searchMovie(page: Int ,query_term : String)
}

extension service : TargetType
{
    
    public var baseURL: URL { return URL(string: "https://yts.am/api/v2")! }
    
    public var path: String {
        switch self {
        case .listMovies( _ ):
            return "/list_movies.json"
        case .searchMovie( _, _):
            return "/list_movies.json"
        }
    }
    
    public var method: Moya.Method {
        switch self {
        default:
            return .get
        }
    }
    

    
    public var task: Task {
        switch self {
        default:
            return .requestParameters(parameters: parameters, encoding: URLEncoding.default)        }
    }
    
    public var parameters: [String: Any] {
        switch self {
        case .listMovies(let page):
            return ["page": page]
        case .searchMovie(let page, let query_term):
            return ["page": page  , "query_term" : query_term]
        }
    }
    


    
    // related to testing
    public var sampleData: Data {
        switch self {
        case .listMovies(let page):
            let moviesJson = [
                "page" : page
            ]
            return jsonSerializedUTF8(json : moviesJson)
        case .searchMovie(let page, let query_term):
            return "{\"query_term\": \"\(query_term)\",\"page\": \(page)}".data(using: String.Encoding.utf8)!
        }
    }
    
    public var headers: [String: String]? {
        return nil
    }
}


// MARK: - Response Handlers
extension Moya.Response {
    func mapNSArray() throws -> NSArray {
        let any = try self.mapJSON()
        guard let array = any as? NSArray else {
            throw MoyaError.jsonMapping(self)
        }
        return array
    }
}

private func jsonSerializedUTF8(json: [String: Any]) -> Data {
    return try! JSONSerialization.data(
        withJSONObject: json,
        options: [.prettyPrinted]
    )
}

