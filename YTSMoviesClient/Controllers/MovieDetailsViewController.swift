//
//  MovieDetailsViewController.swift
//  YTSMoviesClient
//
//  Created by Wafaa Farrag  on 1/4/18.
//  Copyright © 2018 Wafaa Farrag . All rights reserved.
//

import UIKit
import Cosmos
import Agrume

class MovieDetailsViewController: UIViewController {

    @IBOutlet var movieImageView: UIImageView!
    @IBOutlet var movieRate: CosmosView!
    @IBOutlet var summaryTextView: UITextView!
    @IBOutlet var movieDateUploaded: UILabel!
    
    var movie : Movie!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.prepare()
    }

    @IBAction func openUrlMovieAction() {
        guard let url = URL(string: movie.url) else {
            return
        }
        if #available(iOS 10.0, *) {
            UIApplication.shared.open(url, options: [:], completionHandler: nil)
        } else {
            UIApplication.shared.openURL(url)
        }
    }
    
    
    /// set content view
    func prepare()
    {
        if HelperMethods.getTypeOfDevice() != 1
        {
            self.movieRate.settings.starSize = 40
        }
        self.movieRate.rating = Double(self.movie.rating)
        self.navigationItem.title = movie.title
        if let url = URL(string: movie.mediumCoverImage)
        {
            movieImageView.kf.setImage(with: url)
            let tapGesture = UITapGestureRecognizer(target: self, action: #selector(tap(_:)))
            self.movieImageView.addGestureRecognizer(tapGesture)
        }
        self.movieDateUploaded.text = movie.dateUploaded
       // self.summaryTextView.layer.borderWidth = 1
        self.summaryTextView.layer.cornerRadius = 8
        self.summaryTextView.text = movie.summary
    }
    
    @objc
    func tap(_ sender: UIGestureRecognizer){
        if let im = ((sender.view)as! UIImageView ).image
        {
            let agrume = Agrume(image: im)
            agrume.showFrom(self)
        }
    }

}
