//
//  MoviesViewController.swift
//  YTSMoviesClient
//
//  Created by Wafaa Farrag  on 12/28/17.
//  Copyright © 2017 Wafaa Farrag . All rights reserved.
//

import UIKit
import KRPullLoader
import NVActivityIndicatorView


class MoviesViewController: UIViewController {

    @IBOutlet var movieViewModel: MoviesViewModel!
    @IBOutlet var searchBar: UISearchBar!
    @IBOutlet var tableView: UITableView!
    
    private var activityIndicatorView : NVActivityIndicatorView!
    private var canLoadMoreMovies = true
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.register(UINib(nibName: "MovieTableViewCell", bundle: nil), forCellReuseIdentifier: "movieCell")
        self.setupLoadMore()
        self.loaderSetting()
        movieViewModel.getMovies(completion: {[unowned self] movieCount in
            self.activityIndicatorView.stopAnimating()
            guard movieCount == 0 else
            {
                self.movieViewModel.currentMovieArray = self.movieViewModel.moviesArray
                self.tableView.reloadData()
                return
            }
            self.canLoadMoreMovies = false
        },
                                 error: {
            self.activityIndicatorView.stopAnimating()
            self.showAlert(message: "Unknown error", title: "")
        })
    }
}


// MARK: - helper methods
extension MoviesViewController
{
    /// setup loadeMoreView and refreshView
    func setupLoadMore()
    {
        let refreshView = KRPullLoadView()
        refreshView.delegate = self
        tableView.addPullLoadableView(refreshView, type: .refresh)
        let loadMoreView = KRPullLoadView()
        loadMoreView.delegate = self
        tableView.addPullLoadableView(loadMoreView, type: .loadMore)
    }
    
    
    /// loader setting
    func loaderSetting()
    {
        let frame = CGRect(x: (self.view.center.x - 25 ), y: (self.view.center.y - 25), width: 50 , height: 50)
        self.activityIndicatorView = NVActivityIndicatorView(frame: frame,
                                                            type: NVActivityIndicatorType(rawValue: 5))
        self.activityIndicatorView.color = .blue
        self.view.addSubview(activityIndicatorView)
        self.activityIndicatorView.startAnimating()
    }
    
    
    /// search request
    func  searchByUsingWebService()  {
         self.activityIndicatorView.startAnimating()
        self.movieViewModel.searchWithQuery( queryTerm: searchBar.text! , completion: {[unowned self] movieCount in
         self.activityIndicatorView.stopAnimating()
         if movieCount != 0
         {
            self.tableView.reloadData()
         }
         }, error: {
         self.activityIndicatorView.stopAnimating()
         self.showAlert(message: "Unknown error", title: "")
         })
    }
    
    /// search in local movies array
    func searchByUsingFilter() {
        self.movieViewModel.searchByUsingFilter(query: searchBar.text!, completion: {
            self.tableView.reloadData()
        })
    }
    
    
    /// show alert
    ///
    /// - Parameters:
    ///   - message: message of alert
    ///   - title: title of alert
    func showAlert(message : String , title : String)
    {
        let alert = UIAlertController(title: title, message: message , preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: { action in
            switch action.style{
            case .default:
                print("default")
                
            case .cancel:
                print("cancel")
                
            case .destructive:
                print("destructive")
            }}))
       self.present(alert, animated: true, completion: nil)
    }

}

// MARK: - UITableViewDataSource , UITableViewDelegate
extension MoviesViewController : UITableViewDataSource , UITableViewDelegate
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return (self.movieViewModel.currentMovieArray?.count) ?? 0
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
       let destination = self.storyboard?.instantiateViewController(withIdentifier: "MovieDetailsViewController") as! MovieDetailsViewController
        destination.movie = self.movieViewModel.currentMovieArray![indexPath.row]
        self.navigationController?.pushViewController(destination, animated: true)
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "movieCell", for: indexPath) as! MovieTableViewCell
        cell.setContent(from: self.movieViewModel.currentMovieArray![indexPath.row])
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        var height : CGFloat = 120
        if( HelperMethods.getTypeOfDevice() != 1)
        {
            height = 150
        }
        return height
    }
}


// MARK: - KRPullLoadView delegate -------------------
extension MoviesViewController : KRPullLoadViewDelegate {
    func pullLoadView(_ pullLoadView: KRPullLoadView, didChangeState state: KRPullLoaderState, viewType type: KRPullLoaderType) {
        if type == .loadMore {
            switch state {
            case let .loading(completionHandler):
                DispatchQueue.main.asyncAfter(deadline: DispatchTime.now()+1) {
                    completionHandler()
                    if self.canLoadMoreMovies
                    {
                        self.movieViewModel.page += 1
                        self.movieViewModel.getMovies(completion: { [unowned self] moviesCount in
                            guard moviesCount != 0 else
                            {
                                self.canLoadMoreMovies = false
                                return
                            }
                            self.searchBar.text = ""
                            self.movieViewModel.currentMovieArray = self.movieViewModel.moviesArray
                            self.tableView.reloadData()
                        }, error: {
                     self.showAlert(message: "Unknown error", title: "")
                        })
                    }
                }
            default: break
            }
            return
          }
        
            switch state {
                 case .none:
                     pullLoadView.messageLabel.text = ""
         
                 case let .pulling(offset, threshould):
                      if offset.y > threshould {
                      pullLoadView.messageLabel.text = "Pull more."
                    } else {
                      pullLoadView.messageLabel.text = "Release to refresh."
                    }
            
                 case let .loading(completionHandler):
                     pullLoadView.messageLabel.text = "Updating..."
                     DispatchQueue.main.asyncAfter(deadline: DispatchTime.now()+1) {
                     completionHandler()
                     self.movieViewModel.page = 1
                     self.movieViewModel.getMovies(completion: { [unowned self] moviesCount in
                     guard moviesCount != 0 else
                      {
                        return
                       }
                      self.movieViewModel.currentMovieArray = self.movieViewModel.moviesArray
                      self.searchBar.text = ""
                      self.tableView.reloadData()
                      }, error: {
                      self.showAlert(message: "Unknown error", title: "")
                    } , refresh: true)
                }
            }
         }
       }

// MARK: - UISearchBarDelegate
extension MoviesViewController : UISearchBarDelegate
{
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        guard !searchText.isEmpty else
        {
            self.movieViewModel.page = 1
            self.movieViewModel.currentMovieArray = self.movieViewModel.moviesArray
            self.activityIndicatorView.stopAnimating()
            self.tableView.reloadData()
            return
        }
        self.searchByUsingWebService()
       //or use search with local array
       //self.searchByUsingFilter()
    
    }

    
}
