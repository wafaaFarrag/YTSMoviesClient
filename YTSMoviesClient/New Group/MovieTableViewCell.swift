//
//  MovieTableViewCell.swift
//  YTSMoviesClient
//
//  Created by Wafaa Farrag  on 1/4/18.
//  Copyright © 2018 Wafaa Farrag . All rights reserved.
//

import UIKit
import Cosmos
import Kingfisher

class MovieTableViewCell: UITableViewCell {

    @IBOutlet var movieNameLabel: UILabel!
    @IBOutlet var movieImageView: UIImageView!
    @IBOutlet var rateMovieView: CosmosView!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
       if HelperMethods.getTypeOfDevice() != 1
       {
        self.rateMovieView.settings.starSize = 40
       }
    }
    
    func setContent(from movieModel : Movie)  {
        
        self.rateMovieView.rating = Double(movieModel.rating)
        self.movieNameLabel.text! = movieModel.title
        let url = URL(string: movieModel.mediumCoverImage)
        movieImageView.kf.setImage(with: url)
        
    }
    
}
